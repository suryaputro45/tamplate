<style>

</style>

<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">

        <div class="sidebar-brand">
            <a href="/">
                <img src="../../assets/img/logo.png" alt="Logo" height="60px" class="mt-2 mb-2">
                <div class="logo-text mt-2">
                    <span class="app-name">Kabupaten</span>
                    <span class="app-client-name"> Lampung Selatan</span>
                </div>

            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">
                <img src="../../assets/img/logo.png" alt="Logo" height="50px" class="mt-2 mb-2">
            </a>
        </div>
        <ul class="sidebar-menu bg-gray">
            <!-- <li class="menu-header">Dashboard</li> -->
            <li class="dropdown active bg-gray">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Configurasi</span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a class="nav-link" href="index-0.html">Menu</a></li>
                    <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                </ul>
            </li>

            <li><a class="nav-link" href="credits.html"><i class="fas fa-pencil-ruler"></i> <span>Credits</span></a></li>
        </ul>
    </aside>
</div>